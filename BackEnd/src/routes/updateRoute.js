const express = require("express");
const router = express.Router();
const { authorizeUser } = require("../controllers/authControllers");
const { updateUser } = require("../models/user");

router.put("/:username", authorizeUser, async (req, res) => {
  const { username } = req.params;
  const updatedUser = req.body;
  const age = updatedUser.age;
  try {
    const user = await updateUser(username, updatedUser);

    if (age < "18") {
      res.status(409).json({ error: "Age should be above 18!" });
    } else if (user) {
      res.json(user);
    } else {
      res.status(404).json({ error: "User not found" });
    }
  } catch (error) {
    res.status(500).json({ error: "Internal Server Error" });
  }
});

module.exports = router;
