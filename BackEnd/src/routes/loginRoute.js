const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const jwtSecret = require("../controllers/tokenController");
const { check, validationResult } = require("express-validator");
const { authenticateUser } = require("../controllers/authControllers");

const validateLogin = [
  check("email").isEmail().withMessage("Invalid Email!").trim(),
  check("password")
    .isLength({ min: 8 })
    .withMessage("Invalid Password!")
    .trim(),
];

router.post("/", validateLogin, async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(409).json({
      error: "Invalid login credentials!",
      errors: errors.array(),
    });
  }
  try {
    const { email, password } = req.body;
    const user = await authenticateUser(email, password);
    if (user) {
      const token = jwt.sign({ username: user.username }, jwtSecret);
      res.json({ user, token });
    } else {
      res.status(409).json({ error: errors.msg });
    }
  } catch (error) {
    res.status(500).json("Internal Server Error!");
  }
});

module.exports = router;
