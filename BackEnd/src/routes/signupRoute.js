const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");
const { check, validationResult } = require("express-validator");
const {
  createUser,
  findUserByEmail,
  findUserByUsername,
} = require("../models/user");

const validateSignup = [
  check("username")
    .notEmpty()
    .withMessage("Username should be between Aa-Zz and 0-9")
    .trim(),
  check("email").isEmail().withMessage("Invalid email format!").trim(),
  check("password")
    .isLength({ min: 8 })
    .withMessage("Password must contain minimum 8 characters!")
    .trim(),
];

router.post("/signup", validateSignup, async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(409).json({
      error: "All the Fields are required *",
      errors: errors.array(),
    });
  }

  try {
    const { username, email, password } = req.body;
    const existingEmail = await findUserByEmail(email);
    const existingUsername = await findUserByUsername(username);
    if (existingEmail && existingUsername) {
      return res
        .status(409)
        .json({ error: "Email and Username already exist!" });
    } else if (existingUsername) {
      return res.status(409).json({ error: "Username already exists!" });
    } else if (existingEmail) {
      return res.status(409).json({ error: "Email already exists!" });
    }

    hashedPassword = await bcrypt.hash(password, 10);
    const user = await createUser(username, email, hashedPassword);
    res.status(201).json({ message: "User Created Successfully", user });
  } catch (err) {
    res.status(500).json({ error: "Internal Server Error" });
  }
});

function isValidName(username) {
 const namePattern = /^[A-Za-z]+[0-9]*$/;
 return namePattern.test(username)

}

router.get("/checkUsername/:username", async (req, res) => {
  const { username } = req.params;
  const userExists = await findUserByUsername(username);
  const nameCheck = await isValidName(username)
  res.json({ exists: userExists, pattern: nameCheck });
});

function isEmailValid(email) {
  const emailPattern = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
  return emailPattern.test(email);
}

router.get("/checkEmail/:email", async (req, res) => {
  const { email } = req.params;
  const emailCheck = await isEmailValid(email);
  const emailExists = await findUserByEmail(email);
  res.json({ exists: emailExists, pattern: emailCheck });
});

module.exports = router;
