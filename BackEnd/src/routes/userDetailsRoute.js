const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken")
const { getAllUserDetails } = require("../models/user");
const jwtSecret = require("../controllers/tokenController");

function authenticateJWT(req, res, next) {
  const token = req.body.headers["Authorization"];

  if (!token) {
    return res.status(401).json({ error: "Unauthorized access" });
  }

  jwt.verify(token, jwtSecret, (err, user) => {
    if (err) {
      return res.status(403).json({ error: "Token verification failed" });
    }
    req.user = user;
    next();
  });
}

router.post("/users", authenticateJWT, async (req, res) => {
  try {
    const users = await getAllUserDetails();

    if (users.length > 0) {
      const userData = users.map(({ password, ...userData }) => userData);
      res.json({ userData });
    } else {
      res.status(404).json({ error: "No users found" });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

module.exports = router;
