const express = require("express");
const { findUserByEmail, updatePassword } = require("../models/user");

const router = express.Router();

router.post("/update-password", async (req, res) => {
  const { email, newPassword, confirmPassword } = req.body;

  try {
    const user = await findUserByEmail(email);

    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    if (newPassword !== confirmPassword) {
      return res
        .status(409)
        .json({ message: "New password and confirm password do not match" });
    }

    await updatePassword(email, newPassword);
    res.status(200).json({ message: "Password updated successfully" });
  } catch (error) {
    console.error("Error updating password:", error);
    res.status(500).json({ message: "Error updating password" });
  }
});

module.exports = router;
