const crypto = require("crypto");
function createToken() {
  const jwtSecret = crypto.randomBytes(32).toString("hex");
  return jwtSecret;
}

module.exports = createToken();
