const { findUserByEmail } = require("../models/user");
const bcrypt = require("bcrypt");
const Crypto = require("crypto-js");

async function authenticateUser(email, password) {
  let user = await findUserByEmail(email);
  if (!user) {
    return null;
  }
  const secretKey = "a1b2c3d4e5f6g7h8";
  const iv = "e7cfe5f4d4bcd718306d1c1d219dfc93";
  const encryptedPassword = password;
  let decryptedPassword = Crypto.AES.decrypt(encryptedPassword, secretKey,{iv}).toString(Crypto.enc.Utf8);
  const passwordMatch = await bcrypt.compareSync(decryptedPassword, user.password);
  if (passwordMatch) {
    user = {username: user.username}
    return user;
  } else {
    return null;
  }
}

function authorizeUser(req, res, next) {
  const username = req.params.username;
  if (username) {
    next();
  } else {
    res.status(403).json({ msg: "Unauthorized access" });
  }
}

module.exports = {
  authenticateUser,
  authorizeUser,
};
