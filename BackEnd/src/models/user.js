const sqlite3 = require("sqlite3");
const bcrypt = require("bcrypt");

const db = new sqlite3.Database("users.db");

db.serialize(() => {
  db.run(`
    CREATE TABLE IF NOT EXISTS users (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      username TEXT UNIQUE NOT NULL,
      email TEXT UNIQUE NOT NULL,
      password TEXT NOT NULL,
      age INTEGER CHECK (age >= 18),
      dob DATE,
      qualification TEXT,
      office TEXT,
      role TEXT,
      mobile NUMERIC CHECK (mobile >= 1000000000 AND mobile <= 9999999999)
    )
  `);
});

async function createUser(username, email, password) {
  return new Promise((resolve, reject) => {
    db.run(
      "INSERT INTO users (username, email, password) VALUES (?, ?, ?)",
      [username, email, password],
      function (err) {
        if (err) {
          reject(err);
        } else {
          resolve({ id: this.lastID });
        }
      }
    );
  });
}

function findUserByEmail(email) {
  return new Promise((resolve, reject) => {
    db.get("SELECT * FROM users WHERE email = ?", [email], (err, row) => {
      if (err) {
        reject(err);
      } else {
        resolve(row);
      }
    });
  });
}

function findUserByUsername(username) {
  return new Promise((resolve, reject) => {
    db.get("SELECT * FROM users WHERE username = ?", [username], (err, row) => {
      if (err) {
        reject(err);
      } else {
        resolve(row);
      }
    });
  });
}

function getAllUserDetails() {
  return new Promise((resolve, reject) => {
    const query = "SELECT * FROM users";
    db.all(query, [], (err, rows) => {
      if (err) {
        reject(err);
      } else {
        resolve(rows);
      }
    });
  });
}

async function updateUser(username, updatedUserData) {
  const { email, age, dob, qualification, office, role, mobile } =
    updatedUserData;
  if (age < "18") {
    return null;
  }
  return new Promise((resolve, reject) => {
    db.run(
      `
      UPDATE users
      SET  email = ?, age =? , dob =?,qualification = ?, office = ?, role = ?, mobile = ?
      WHERE username = ?
    `,
      [email, age, dob, qualification, office, role, mobile, username],
      function (err) {
        if (err) {
          reject(err);
        } else {
          resolve(updatedUserData);
        }
      }
    );
  });
}

async function updatePassword(email, newPassword) {
  const hashedPassword = await bcrypt.hash(newPassword, 10);
  return new Promise((resolve, reject) => {
    db.run(
      `
      UPDATE users
      SET password = ?
      WHERE email = ?
    `,
      [hashedPassword, email],
      function (err) {
        if (err) {
          reject(err);
        } else {
          resolve("Password updated successfully");
        }
      }
    );
  });
}

module.exports = {
  createUser,
  findUserByEmail,
  findUserByUsername,
  getAllUserDetails,
  updateUser,
  updatePassword,
};
