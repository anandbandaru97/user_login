const express = require("express");
const cors = require("cors");
require("dotenv").config();

const signupRoute = require("./routes/signupRoute");
const loginRoute = require("./routes/loginRoute");
const userDetailsRoute = require("./routes/userDetailsRoute");
const updateRoute = require("./routes/updateRoute");
const resetPasswordRoute = require("./routes/resetPasswordRoute.js");
const app = express();
const { PORT } = process.env;

app.use(express.json());
app.use(cors({ origin: "*" }));

app.use("/api/user", signupRoute);
app.use("/api/user", loginRoute);
app.use("/api/user", userDetailsRoute);
app.use("/api/user", updateRoute);
app.use("/api/user", resetPasswordRoute);

app.listen(PORT, () => {
  console.log(`Server is running on PORT ${PORT}`);
});
