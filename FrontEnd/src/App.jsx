import React from "react";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import Header from "./components/Header";
import Login from "./components/Login";
import SignUp from "./components/SignUp";
import User from "./components/User";
import ResetPassword from "./components/ResetPassword";
import Unauthorized from "./components/Unauthorized";

const App = () => {
  const isUserLoggedIn = () => {
    const token = localStorage.getItem("token");
    if (!token) {
      return false;
    }
    return true;
  };

  const PrivateUserRoute = ({ element }) => {
    return isUserLoggedIn() ? element : <Navigate to="/unauthorized" />;
  };

  return (
    <BrowserRouter>
      <Header />
      <Routes>
        <Route exact path="/" element={<Login />} />
        <Route exact path="/signup" element={<SignUp />} />
        <Route exact path="/resetpassword" element={<ResetPassword />} />
        <Route
          path="/user/:username"
          element={<PrivateUserRoute element={<User />} />}
        />
        <Route path="/unauthorized" element={<Unauthorized />} />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
