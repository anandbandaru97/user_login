import React from "react";
import { Typography } from "@mui/material";

const Header = () => {
  return (
    <header>
      <img
        className="logo"
        src="https://varthana.com/school/wp-content/uploads/2021/02/cropped-Varthana-Logo.png"
      />
      <Typography variant="h4">Varthana</Typography>
    </header>
  );
};

export default Header;
