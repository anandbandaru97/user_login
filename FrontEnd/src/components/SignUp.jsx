import React, { useState } from "react";
import {
  Card,
  Typography,
  Button,
  TextField,
  IconButton,
  Alert,
  Grid,
} from "@mui/material";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import { Visibility, VisibilityOff } from "@mui/icons-material";

const apiUrl = "http://localhost:7000/api/user/signup";

const SignUp = () => {
  const [userData, setUserData] = useState({
    username: "",
    email: "",
    password: "",
  });
  const [usernameExists, setUsernameExists] = useState("");
  const [emailExists, setEmailExists] = useState("");
  const [nameField, setNameField] = useState("");
  const [emailField, setEmailField] = useState("");
  const [passwordCheck, setPasswordCheck] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(null);

  const navigate = useNavigate();

  const createUser = async () => {
    try {
      const res = await axios.post(apiUrl, userData);
      if (res.status === 201) {
        setSuccess("User Created Successfully!");
        setError(null);
        setUserData({ username: "", email: "", password: "" });
        setTimeout(() => {
          navigate(`/`);
        }, 1000);
      }
    } catch (error) {
      if (error.response) {
        const { status, data } = error.response;
        if (status === 409) {
          console.log(data.errors)
          if (!Array.isArray(data.errors)) {
            setError(data.errors[0].msg);
          } else {
            setError(data.error);
          }
        } else {
          setError("User creation failed!");
        }
      }
      console.error(error.message);
      setSuccess(null);
    }
  };

  const handleUsernameBlur = async () => {
    if (userData.username) {
      try {
        const response = await axios.get(
          `http://localhost:7000/api/user/checkUsername/${userData.username}`
        );
        setUsernameExists(response.data.exists);
        setNameField("");
        if(!response.data.pattern) {
          setNameField("Name should be like Anan123 ra12 etc...")
        }
      } catch (error) {
        setUsernameExists("");
      }
    } else if (!userData.username) {
      setNameField("Please enter name!");
    } else {
      setUsernameExists("");
    }
  };

  const handleEmailBlur = async () => {
    if (userData.email) {
      try {
        const response = await axios.get(
          `http://localhost:7000/api/user/checkEmail/${userData.email}`
        );
        setEmailExists(response.data.exists);
        setEmailField("");
        if (!response.data.pattern) {
          setEmailField("Invalid Email!");
        }
      } catch (error) {
        setEmailExists("");
      }
    } else if (!userData.email) {
      setEmailField("Please enter email!");
    } else {
      setEmailExists("");
    }
  };

  const handlePasswordBlur = () => {
    if (userData.password.length < 8) {
      setPasswordCheck(true);
    } else {
      setPasswordCheck(false);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setUserData({
      ...userData,
      [name]: value,
    });
    setError("");
    setSuccess("");
  };

  const handleClickShowPassword = () => {
    setShowPassword((show) => !show);
  };

  const formStyle = {
    padding: "1rem",
  };

  return (
    <div
      style={{
        height: "80vh",
        display: "flex",
        justifyContent: "space-evenly",
        alignItems: "center",
      }}
    >
      <Card sx={formStyle} spacing={2}>
        {success && (
          <Alert variant="filled" severity="success">
            {success}
          </Alert>
        )}
        {error && (
          <Alert variant="filled" severity="error">
            {error}
          </Alert>
        )}
        <Typography variant="h3" sx={{ paddingBottom: "1rem" }}>
          Sign Up
        </Typography>
        <form
          style={{
            display: "flex",
            flexDirection: "column",
            minHeight: "17rem",
            width: "15rem",
          }}
        >
          <Grid container spacing={2}>
            <Grid item>
              {usernameExists && (
                <Typography style={{ color: "red" }}>
                  Username already exists!
                </Typography>
              )}
              {nameField && (
                <Typography style={{ color: "red" }}>
                  {nameField}
                </Typography>
              )}
              <TextField
                label="User Name"
                placeholder="Please Enter Name"
                name="username"
                value={userData.username}
                required
                onChange={handleChange}
                onSelect={handleUsernameBlur}
              />
            </Grid>
            <Grid item>
              {emailExists && (
                <Typography style={{ color: "red" }}>
                  Email already exists!
                </Typography>
              )}
              {emailField && (
                <Typography style={{ color: "red" }}>{emailField}</Typography>
              )}
              <TextField
                label="Email"
                placeholder="Please Enter Email"
                name="email"
                value={userData.email}
                required
                onChange={handleChange}
                onSelect={handleEmailBlur}
              />
            </Grid>
            <Grid item>
              {passwordCheck && (
                <Typography style={{ color: "red" }}>
                  Password is too short!
                </Typography>
              )}
              <TextField
                label="Password"
                placeholder="Please Enter Password"
                style={{ width: "14.7rem" }}
                name="password"
                value={userData.password}
                required
                onChange={handleChange}
                onSelect={handlePasswordBlur}
                type={showPassword ? "text" : "password"}
                InputProps={{
                  endAdornment: (
                    <IconButton
                      // aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      edge="end"
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  ),
                }}
              />
            </Grid>
            <Grid item>
              <Button
                onClick={createUser}
                variant="contained"
                style={{ width: "10rem" }}
              >
                SignUp
              </Button>
            </Grid>
            <Grid item>
              <Link to="/">
                <Button>Back</Button>
              </Link>
            </Grid>
          </Grid>
        </form>
      </Card>
    </div>
  );
};

export default SignUp;
