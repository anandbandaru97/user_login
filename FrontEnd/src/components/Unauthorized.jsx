import { Alert } from "@mui/material";
import React from "react";

const Unauthorized = () => {
  return (
    <Alert variant="filled" severity="error">
      Unauthorized Access!
    </Alert>
  );
};

export default Unauthorized;
