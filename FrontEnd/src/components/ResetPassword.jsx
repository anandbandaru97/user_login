import React, { useState } from "react";
import {
  Alert,
  Box,
  Grid,
  TextField,
  Typography,
  Button,
  IconButton,
  Card,
} from "@mui/material";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { Visibility, VisibilityOff } from "@mui/icons-material";

const apiUrl = "http://localhost:7000/api/user/update-password";

function ResetPassword() {
  const [email, setEmail] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [success, setSuccess] = useState(null);
  const [error, setError] = useState(null)
  const navigate = useNavigate();

  const handleResetPassword = async () => {
    try {
      const response = await axios.post(apiUrl, {
        email: email,
        newPassword: newPassword,
        confirmPassword: confirmPassword,
      });

      if (response.status === 200) {
        setSuccess("User updated successfully!");
        setEmail("");
        setNewPassword("");
        setConfirmPassword("");
        setTimeout(() => {
          navigate("/");
        }, 1000);
      }
    } catch (error) {
      if (error.response) {
        const errorMessage = error.response.data.message;
        setError(`User update failed!: ${errorMessage}`);
      } 
    }
  };

  const handleClickShowPassword = () => {
    setShowPassword((show) => !show);
  };

  return (
    <div
      style={{
        height: "80vh",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-evenly",
        alignItems: "center",
      }}
    >
      <Card sx={{ padding: ".5rem" }}>
        <Typography variant="h5">Password Reset</Typography>
        {success && (
          <Alert variant="filled" severity="success">
            {success}
          </Alert>
        )} {(error &&
          <Alert variant="filled" severity="error">
            {error}
          </Alert>
        )}
        <Box sx={{ margin: ".5rem" }}>
          <div style={{ paddingBottom: "1rem" }}>
            <TextField
              label="Email"
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div style={{ paddingBottom: "1rem" }}>
            <TextField
              label="New Password"
              style={{ width: "14.5rem" }}
              type={showPassword ? "text" : "password"}
              value={newPassword}
              onChange={(e) => setNewPassword(e.target.value)}
              InputProps={{
                endAdornment: (
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    edge="end"
                  >
                    {showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                ),
              }}
            />
          </div>
          <div style={{ paddingBottom: "1rem" }}>
            <TextField
              label="Confirm Password"
              style={{ width: "14.5rem" }}
              type={showPassword ? "text" : "password"}
              value={confirmPassword}
              onChange={(e) => setConfirmPassword(e.target.value)}
              InputProps={{
                endAdornment: (
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    edge="end"
                  >
                    {showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                ),
              }}
            />
          </div>
          <div>
            <div>
              <Button variant="contained" onClick={handleResetPassword}>
                Reset Password
              </Button>
            </div>
          </div>
        </Box>
      </Card>
    </div>
  );
}

export default ResetPassword;
