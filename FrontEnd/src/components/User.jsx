import React, { useEffect, useState } from "react";
import {
  Alert,
  Card,
  Typography,
  Button,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Pagination,
  Stack,
} from "@mui/material";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";
import UpdateUser from "./UpdateUser";

const User = () => {
  const { username } = useParams();
  const navigate = useNavigate();
  const [users, setUsers] = useState([]);
  const [success, setSuccess] = useState("");
  const [error, setError] = useState(null);
  const [page, setPage] = useState(1);

  const handleClick = () => {
    localStorage.removeItem("token");
    navigate("/");
  };
  const apiUrl = `http://localhost:7000/api/user/users`;
  let token = localStorage.getItem("token");
  useEffect(() => {
    if (token) {
      const fetchData = async () => {
        try {
          const res = await axios.post(apiUrl, {
            headers: {
              Authorization: `${token}`,
            },
          });
          const { userData } = res.data;
          if (userData) {
            setUsers(userData);
          } else {
            setError("Failed to fetch data");
          }
          localStorage.removeItem("token");
        } catch (error) {
          setError("Unauthorized Access!");
        }
      };
      fetchData();
    }
  }, []);

  const handleUpdateUser = (updatedUser) => {
    const index = users.findIndex((user) => user.email === updatedUser.email);
    if (index !== -1) {
      const updatedUsers = [...users];
      updatedUsers[index] = updatedUser;
      setUsers(updatedUsers);
      setSuccess(`${updatedUser.username} details updated successfully!`);
      setTimeout(() => {
        setSuccess("");
      }, 1000);
    }
  };

  const itemsPerPage = 10;
  const handlePageChange = (e, p) => {
    setPage(p);
  };

  const startIndex = (page - 1) * itemsPerPage + 1;
  const endIndex = startIndex + itemsPerPage;
  const pages = Math.ceil(users.length / itemsPerPage);
  const usersToDisplay = users.slice(startIndex - 1, endIndex);

  const userStyle = {
    padding: "1rem",
    display: "flex",
    justifyContent: "space-between",
  };

  const tableItems = {
    height: "72vh",
    padding: "1rem",
    overflow: "auto",
  };

  if (true) {
    history.pushState(null, null, location.href);
    window.onpopstate = function (e) {
      history.go(1);
    };
  }

  if (error) {
    return (
      <Alert variant="filled" severity="error">
        {error}
      </Alert>
    );
  }
  let index = startIndex - 1;
  return (
    <>
      <Card sx={userStyle}>
        <Typography variant="h5">Welcome, {username}!</Typography>
        {success && (
          <Alert variant="filled" severity="success">
            {success}
          </Alert>
        )}
        <Button color="error" onClick={handleClick}>
          Logout
        </Button>
      </Card>
      <TableContainer sx={tableItems}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>S.No</TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Email</TableCell>
              <TableCell>Age</TableCell>
              <TableCell>DOB</TableCell>
              <TableCell>Qualification</TableCell>
              <TableCell>Office</TableCell>
              <TableCell>Role</TableCell>
              <TableCell>Mobile</TableCell>
              <TableCell>Edit Info</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {usersToDisplay.map((userData) => {
              if (userData.username !== username) {
                index += 1;
                return (
                  <TableRow key={userData.id}>
                    <TableCell>{index}</TableCell>
                    <TableCell>{userData.username}</TableCell>
                    <TableCell>{userData.email}</TableCell>
                    <TableCell>{userData.age}</TableCell>
                    <TableCell>{userData.dob}</TableCell>
                    <TableCell>{userData.qualification}</TableCell>
                    <TableCell>{userData.office}</TableCell>
                    <TableCell>{userData.role}</TableCell>
                    <TableCell>{userData.mobile}</TableCell>
                    <TableCell>
                      <UpdateUser
                        user={userData}
                        updateDetails={handleUpdateUser}
                      />
                    </TableCell>
                  </TableRow>
                );
              } else {
                return null;
              }
            })}
          </TableBody>
        </Table>
        {/* Pagination */}
        <Stack>
          <Pagination
            count={pages}
            style={{ position: "fixed", bottom: ".5rem", right: "46%" }}
            onChange={handlePageChange}
          />
        </Stack>
      </TableContainer>
    </>
  );
};

export default User;
