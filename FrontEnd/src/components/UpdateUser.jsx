import React, { useState } from "react";
import { Button, Card, Modal, TextField, Typography } from "@mui/material";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import axios from "axios";
import enGB from "date-fns/locale/en-GB";
import { format, parse, differenceInYears } from "date-fns";
let changed = false;

const UpdateUser = ({ user, updateDetails }) => {
  const [openModal, setOpenModal] = useState(false);
  const [error, setError] = useState(null);
  const [updateUser, setUpdateUser] = useState({
    username: user.username,
    email: user.email,
    age: user.age,
    dob: user.dob,
    qualification: user.qualification,
    office: user.office,
    role: user.role,
    mobile: user.mobile,
  });
  const [selectedDate, setSelectedDate] = useState(null);
  const apiUrl = `http://localhost:7000/api/user/${user.username}`;

  const handleUpdate = async () => {
    try {
      const response = await axios.put(apiUrl, updateUser);
      updateDetails(response.data);
      handleCloseModal();
    } catch (error) {
      if (error.response) {
        const { status, data } = error.response;
        if (status === 409) {
          setError(data.error);
        }
      }
      console.error("Error updating the user:", error);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setError(null);
    const tenDigitNumberRegExp = /^\d{0,10}$/;
    changed = true;
    if (name !== "mobile" || name !== "dob") {
      setUpdateUser({
        ...updateUser,
        [name]: value,
      });
    }
    if (name === "mobile" && (value.length !== 10 || !value)) {
      setError("Mobile must have 10 digits!");
    } else if (name === "mobile" && tenDigitNumberRegExp.test(value)) {
      setUpdateUser({
        ...updateUser,
        [name]: value,
      });
    }
  };

  const handleDateChange = (date) => {
    const maxDate = new Date("2005-01-01");
    changed = true;
    if (differenceInYears(date, maxDate) > 0) {
      setError("Age should be above 18!");
    } else {
      setError(null);
      setSelectedDate(date);

      const calculateAgeFromDob = (dob) => {
        const today = new Date();
        return differenceInYears(today, dob);
      };

      const age = calculateAgeFromDob(date);

      setUpdateUser({
        ...updateUser,
        dob: format(date, "dd/MM/yyyy"),
        age: age,
      });
    }
  };

  const handleOpenModal = () => {
    setOpenModal(true);
  };

  const handleCloseModal = () => {
    setOpenModal(false);
    changed = false;
  };

  return (
    <>
      <Modal
        open={openModal}
        onClose={handleCloseModal}
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Card sx={{ padding: "2rem", width: "25rem" }}>
          {error && <Typography sx={{ color: "red" }}>{error}</Typography>}
          <div className="fields">
            <TextField
              label="Qualification"
              variant="outlined"
              type="text"
              name="qualification"
              value={updateUser.qualification}
              onChange={handleChange}
            />
          </div>
          <div className="fields">
            <TextField
              label="Office"
              variant="outlined"
              type="text"
              name="office"
              value={updateUser.office}
              onChange={handleChange}
            />
          </div>
          <div className="fields">
            <TextField
              label="Role"
              variant="outlined"
              type="text"
              name="role"
              value={updateUser.role}
              onChange={handleChange}
            />
          </div>
          <div className="fields">
            <TextField
              label="Mobile"
              variant="outlined"
              type="number"
              name="mobile"
              InputProps={{ inputMode: "number", maxLength: 10 }}
              value={updateUser.mobile}
              onChange={handleChange}
            />
          </div>
          <div className="fields">
            <LocalizationProvider
              dateAdapter={AdapterDateFns}
              adapterLocale={enGB}
            >
              <DatePicker
                label="DOB"
                name="dob"
                onChange={(date) => handleDateChange(date)}
                defaultValue={parse(updateUser.dob, "dd/MM/yyyy", new Date())}
                inputFormat="dd/MM/yyyy"
                slotProps={{ TextField: { placeholder: "DD/MM/YYYY" } }}
                sx={{ width: "14.7rem" }}
              />
            </LocalizationProvider>
          </div>
          {changed && (
            <div>
              <Button variant="contained" onClick={handleUpdate}>
                Save
              </Button>
            </div>
          )}
        </Card>
      </Modal>
      <Button variant="contained" onClick={handleOpenModal}>
        Edit
      </Button>
    </>
  );
};

export default UpdateUser;
