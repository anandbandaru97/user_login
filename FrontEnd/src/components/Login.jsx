import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import {
  Button,
  Card,
  TextField,
  Typography,
  IconButton,
  Alert,
  Grid,
} from "@mui/material";
import { Visibility, VisibilityOff } from "@mui/icons-material";
import axios from "axios";
import Crypto from "crypto-js";

const apiUrl = "http://localhost:7000/api/user";

const Login = () => {
  const [userEmail, setUserEmail] = useState("");
  const [userPassword, setPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(null);
  const navigate = useNavigate();

  const handleEmail = (email) => {
    setUserEmail(email);
    setError("");
  };

  const handlePassword = (password) => {
    setPassword(password);
    setError("");
  };

  const handleClickShowPassword = () => {
    setShowPassword((show) => !show);
  };

  const handleLogin = async () => {
    try {
      const secretKey = "a1b2c3d4e5f6g7h8";
      const iv = "e7cfe5f4d4bcd718306d1c1d219dfc93";
      const hashedPassword = Crypto.AES.encrypt(userPassword, secretKey, {
        iv,
      }).toString();

      const response = await axios.post(apiUrl, {
        email: userEmail,
        password: hashedPassword,
      });
      const { user, token } = response.data;
      localStorage.setItem("token", token);
      setSuccess("Login successful");
      setError(null);
      setUserEmail("");
      setPassword("");
      if (user.username) {
        navigate(`/user/${user.username}`);
      } else {
        navigate("/unauthorized");
      }
    } catch (error) {
      if (error.response) {
        const { status, data } = error.response;
        if (status === 401) {
          setError("Unauthorized Login!");
          setSuccess(null);
        }
        if (status === 409) {
          if (!Array.isArray(data.errors)) {
            setError("Invalid Password!");
          } else {
            setError(data.errors[0].msg);
          }
        }
      } else {
        setError(data.error);
      }
    }
  };

  if (true) {
    history.pushState(null, null, location.href);
    window.onpopstate = function (e) {
      history.go(1);
    };
  }

  const formStyle = {
    padding: "1rem",
  };

  const buttonStyle = {
    marginTop: "-.5rem",
  };

  return (
    <div
      style={{
        height: "80vh",
        display: "flex",
        justifyContent: "space-evenly",
        alignItems: "center",
      }}
    >
      <Card sx={formStyle} xs={6} md={8}>
        {success && (
          <Alert variant="filled" severity="success">
            {success}
          </Alert>
        )}
        {error && (
          <Alert variant="filled" severity="error">
            {error}
          </Alert>
        )}
        <Typography variant="h3" sx={{ paddingBottom: "1rem" }}>
          Login
        </Typography>
        <form
          style={{
            display: "flex",
            flexDirection: "column",
            minHeight: "15rem",
            width: "15rem",
          }}
        >
          <Grid container spacing={2}>
            <Grid item>
              <TextField
                label="Email"
                id="email"
                type="email"
                value={userEmail}
                required
                onChange={(e) => handleEmail(e.target.value)}
              />
            </Grid>
            <Grid item>
              <TextField
                label="Password"
                style={{ width: "14.7rem" }}
                id="password"
                type={showPassword ? "text" : "password"}
                value={userPassword}
                required
                onChange={(e) => handlePassword(e.target.value)}
                InputProps={{
                  endAdornment: (
                    <IconButton onClick={handleClickShowPassword} edge="end">
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  ),
                }}
              />
            </Grid>
            <Grid item>
              <Button
                variant="contained"
                onClick={handleLogin}
                style={{ width: "10rem" }}
              >
                Login
              </Button>
            </Grid>
          </Grid>
        </form>
        <div className="buttons">
          <Typography variant="p">Create Account ?</Typography>
          <Link to="/signup">
            <Button sx={buttonStyle}>Sign Up</Button>
          </Link>
        </div>
        <div className="buttons">
          <Typography variant="p">Forgot Password ?</Typography>
          <Link to="/resetpassword">
            <Button sx={buttonStyle}>Reset</Button>
          </Link>
        </div>
      </Card>
    </div>
  );
};

export default Login;
