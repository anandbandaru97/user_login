const data = [
    {
        "id": 1,
        "username": "Anand",
        "email": "anand123@gmail.com",
        "password": "anand123",
        "age": "25 Yrs",
        "dob": "08/06/1997",
        "qualification": "B.Tech",
        "office": "Varthana",
        "role": "SDE",
        "mobile": "7013080107"
    },
    {
        "id": 2,
        "username": "Charan",
        "email": "charan123@gmail.com",
        "password": "charan123",
        "age": "21 Yrs",
        "dob": "18/05/2001",
        "qualification": "B.Tech",
        "office": "Varthana",
        "role": "SDE",
        "mobile": "9100454391"
    },
    {
        "id": 3,
        "username": "Shubam",
        "email": "shubam123@gmail.com",
        "password": "shubam123",
        "age": "27 Yrs",
        "dob": "01/01/1995",
        "qualification": "B.Tech",
        "office": "Varthana",
        "role": "Manager",
        "mobile": "7997665530"
    }
]

export default data